# SQL Files
Each file will hold the table structure and some example data
forms.sql will hold a list of available forms
form_elements.sql will hold a list of elements on each form and their structure
form_element_options.sql will hold a list of options for some form elements which present options to the user (i.e. Dropdown menus, radios, checkboxes)
form_submissions.sql will hold the form submissions

# Example queries
## this will be cleaned up but you should be able to get a general idea


	public function get_forms()
	{
		$DB = $this->load->database("MHC", TRUE);
		$DB->select("formsid, title, category, posturl, fe.id as form_elementsid, label as element_label, helptext as element_helptext,
placeholder as element_placeholder, required as element_required, validation as element_validation, elementtype,
feo.id as elementoptions_id,
feo.[value] as elementoptions_value,
feo.[name] as elementoptions_name");
		$DB->from('forms as f');
		$DB->join('form_elements as fe', 'f.id = fe.formsid');
		$DB->join('form_element_options as feo', 'fe.id = feo.form_elementsid', 'left');
		$DB->where('f.active', 1);
		$DB->order_by('formsid, fe.id');
		$query = $DB->get();
		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}

	}

	public function putFormSubmission($data)
	{
		$DB = $this->load->database("MHC", TRUE);
		$DB->insert('form_submissions', $data);

		return array('success' => $DB->insert_id(), 'submitted_data' => $data);
	}
