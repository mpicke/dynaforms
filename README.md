# DynaForms Purpose and Goals
The purpose is this project is to load form structure and other form settings from JSON, store that form structure and use it to present a list of available forms to the user.  The user will then submit their form entries to an endpoint which is assigned per form.  The primary goal for this project are:

1. To enable table driven form creation and form submittion capture

2. To be Offline by default (to accommodate when a Cellular Network or WiFi are not available.)

This purpose of this project is to enable anyone to be able to incorporate this into their Android application. The purposes might include; Surveys, Report entry, Signature capture, and more.  

The purpose for which the Author will be using this module is to enable Point of Care form entry for Nurses and Physicians when visiting Patients in their homes.  

# Examples and endpoints to use for testing
## Example test endpoint:
[https://www.myhospicecenter.com/app/v1/getforms](https://www.myhospicecenter.com/app/v1/getforms)

## Example JSON return:
> [{"formsid":1,"title":"Who Are You?","category":"People","posturl":"https:\/\/www.myhospicecenter.com\/v1\/put_form","form_elementsid":1,"element_label":"Name","element_helptext":"Here is where you put your name","element_placeholder":"Bob","element_required":0,"element_validation":null,"elementtype":"EditText","elementoptions_id":null,"elementoptions_value":null,"elementoptions_name":null},.....]

## Example Form submission endpoint: 
[https://www.myhospicecenter.com/app/v1/putform](https://www.myhospicecenter.com/app/v1/putform)
GET Method is used for form submission and the variables expected are:
* submitdata (JSON)
* createdate (Date of User Submission)
* username (Name of Submitter)

If successful a JSON message will show you the inserted form entry id and also the data submitted
##example successful result: 
> {"success":123,"submitted_data":{"submittedfield1":value,"submittedfield2":value...}}